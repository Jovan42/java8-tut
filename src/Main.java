import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        //        Person p1 = new Person(25, "A", "A");
        //        Person p2 = new Person(24, "B", "B");
        //
        //        List<Person> persons = new ArrayList<>();
        //        persons.add(p1);
        //        persons.add(p2);
        //
        //        Comparator<Person> cmpAge =
        //                Comparator.comparing(Person::getFirstName)
        //                        .thenComparing(Person::getLastName)
        //                        .thenComparing(Person::getAge);

        //        Predicate<String> p1 = s -> s.length() < 20;
        //        Predicate<String> p2 = s -> s.length() > 5;
        //
        //        Predicate<String> p3 = p1.and(p2);
        //        Predicate<String> p4 = p1.or(p2);
        //
        //        Predicate<String> p5 = Predicate.isEqualsTo("Yes");
        //        Predicate<Integer> p6 = Predicate.isEqualsTo(10);

        List<Integer> ints = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        ints.stream().forEach(System.out::println);

        Stream.generate(() -> "a").limit(10).forEach(System.out::println);
    }

    public static String f(String str, Function<String, String> function) {
        return function.apply(str);
    }
}
