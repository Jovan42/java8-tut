package lambda02;

@FunctionalInterface
public interface Predicate<T> {

    static <U> Predicate<U> isEqualsTo(U u) {
        return s -> s.equals(u);
    }

    public boolean test(T t);

    public default Predicate<T> and(Predicate<T> p2) {
        return t -> test(t) && p2.test(t);
    }

    public default Predicate<T> or(Predicate<T> p2) {
        return t -> test(t) || p2.test(t);
    }
}
